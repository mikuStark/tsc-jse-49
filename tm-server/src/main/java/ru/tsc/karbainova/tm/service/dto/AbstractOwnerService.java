package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.dto.IOwnerService;
import ru.tsc.karbainova.tm.dto.AbstractOwnerDTOEntity;

public abstract class AbstractOwnerService<E extends AbstractOwnerDTOEntity> extends AbstractService<E> implements IOwnerService<E> {

    @NonNull
    public AbstractOwnerService(@NonNull final IConnectionService connectionService) {
        super(connectionService);
    }
}
